import { FormControl, ValidationErrors } from '@angular/forms';

export function vatCodFiscValidator(c: FormControl, requiredLength: number): ValidationErrors | null {
  const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;

  if (c.value && c.value.length !== requiredLength) {
    return {
      totalLength: {
        requiredLength, currentLength: c.value.length
      }
    }
  }
  if (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) {
    return { alphaNumeric: true }
  }

  return null;
}
