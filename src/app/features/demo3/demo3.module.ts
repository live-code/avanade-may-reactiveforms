import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Demo3RoutingModule } from './demo3-routing.module';
import { Demo3Component } from './demo3.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AnagraficaComponent } from './components/anagrafica.component';
import { CarInfoComponent } from './components/car-info.component';


@NgModule({
  declarations: [
    Demo3Component,
    AnagraficaComponent,
    CarInfoComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    Demo3RoutingModule
  ]
})
export class Demo3Module { }
