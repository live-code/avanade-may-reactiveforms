import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'ava-demo3',
  template: `
    <form action="">
      <div [form]="form" [loader]="config" *ngFor="let config of data"></div>
    </form>
      <form (submit)="save()"
            [formGroup]="form">
        <input
          class="form-control"
          [ngClass]="{
            'is-invalid': form.get('cod')?.invalid, 
            'is-valid': form.get('cod')?.valid
          }"
          type="text" 
          placeholder="codice pratice" formControlName="cod">
        <input *ngIf="form.get('phone')" type="text" formControlName="phone" class="phone" >

        <hr>
        <ava-car-info [form]="form"></ava-car-info>
        <ava-anagrafica [form]="form"></ava-anagrafica>
        
        <button type="submit" [disabled]="form.invalid">Submit</button>
      </form>
      
      <pre>{{form.value | json}}</pre>
      
      
      <button (click)="addField()">add Field</button>
  `,
})
export class Demo3Component {
  form: FormGroup;

  constructor(fb: FormBuilder) {
    this.form = fb.group({
      cod:  ['', Validators.required],
      carInfo: fb.group({
        brand: ['', Validators.required],
        model: ['', Validators.required],
      }),
      anagrafica: fb.group({
        name:  ['', Validators.required],
        surname:  ['', Validators.required],
      })
    })
    setTimeout(() => {
      /*
        const data = {
         cod: '2Fabio',
         carInfo: {
           brand: '2a',
           model: '2b'
         },
         anagrafica: {
           name: '2',
           surname: '2'
         }
       }

       this.form.setValue(data)*/
    }, 2000)

    this.form.get('carInfo')?.valueChanges
      .subscribe(res => {
        if(this.form.get('carInfo')?.valid) {
          this.form.get('anagrafica')?.enable()
        } else {
          this.form.get('anagrafica')?.disable()
        }
      })

  }

  save() {

  }

  addField() {
    this.form.addControl('phone', new FormControl('123'))
  }
}
