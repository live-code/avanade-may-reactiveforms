import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'ava-anagrafica',
  template: `
    <div [formGroup]="form">
      <div formGroupName="anagrafica" >
        <h1>Anagrafica</h1>
        <input type="text" placeholder="name" formControlName="name">
        <input type="text" placeholder="surname" formControlName="surname">
      </div>
    </div>
  `,
  styles: [
  ]
})
export class AnagraficaComponent implements OnInit {
  @Input() form!: FormGroup
  constructor() { }

  ngOnInit(): void {
  }

}
