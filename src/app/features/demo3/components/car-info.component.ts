import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'ava-car-info',
  template: `
    <div [formGroup]="form">
      <div formGroupName="carInfo" [ngClass]="{'alert-success': form.get('carInfo')?.valid}">
        <h1>Info Car {{form.get('carInfo')?.valid}}</h1>
        <input type="text" placeholder="car brand" formControlName="brand"
               class="form-control"
               [ngClass]="{
                      'is-invalid': form.get('carInfo')?.get('brand')?.invalid, 
                      'is-valid': form.get('carInfo')?.get('brand')?.valid
                   }"
        >
        <input type="text" placeholder="car model" formControlName="model">
  
      </div>
    </div>
  `,
  styles: [
  ]
})
export class CarInfoComponent implements OnInit {
  @Input() form!: FormGroup
  constructor() { }

  ngOnInit(): void {
  }

}
