import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-demo4',
  template: `
    <p>
      demo4 works!
    </p>
  `,
  styles: [
  ]
})
export class Demo4Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
