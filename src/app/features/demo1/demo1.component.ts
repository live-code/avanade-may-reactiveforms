import { Component  } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { vatCodFiscValidator } from '../../core/form-utils';

@Component({
  selector: 'ava-demo1',
  template: `
    <form [formGroup]="form" (submit)="save()">
      <div *ngIf="form.get('company')?.errors?.['required']">Compnay name is required</div>
      <div *ngIf="form.get('company')?.errors?.['minlength']">company too short</div>
      <input type="text" formControlName="company" placeholder="Company Name">


      <input type="text" formControlName="vatNumber" placeholder="Vat number">


      <pre>{{form.get('codFisc')?.errors | json}}</pre>
      <input type="text" formControlName="codFisc" placeholder="Cod fisc">
      <button type="submit" [disabled]="form.invalid">SAVE</button>
    </form>
  `,
})
export class Demo1Component  {
  form: FormGroup

  constructor(private fb: FormBuilder) {
    this.form = fb.group({
      company: ['ACME', [Validators.required, Validators.minLength(3)]],
      vatNumber: ['0123456789', [
        Validators.required, (c: FormControl) => vatCodFiscValidator(c, 11)]
      ],
      codFisc: ['0123456789', [
        Validators.required, (c: FormControl) => vatCodFiscValidator(c, 16)]
      ],
    })
  }
  save() {
    console.log(this.form.value)
  }
}
