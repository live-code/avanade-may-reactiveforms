import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-demo5',
  template: `
    <p>
      demo5 works!
    </p>
  `,
  styles: [
  ]
})
export class Demo5Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
