import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { vatCodFiscValidator } from '../../core/form-utils';

@Component({
  selector: 'ava-demo2',
  template: `
    <form [formGroup]="form" (submit)="save()">
      <input type="checkbox" formControlName="isCompany"> are you a company?
      <input type="text" formControlName="company" placeholder="Company Name" class="form-control">
      
      <div class="alert alert-danger">
        <div>
          <div>Vat Number</div>
          <input type="text" formControlName="idNumber"
                 [ngClass]="{'is-invalid': form.get('idNumber')?.invalid && form.dirty}"
                 [placeholder]="form.get('isCompany')?.value ? 'Vat number' : 'cod fisc'" 
                 class="form-control">
        </div>
      </div>
      

      <small>{{form.get('isCompany')?.value ? 'required 11 chars' : 'require 16 chars'}}</small>
      <hr>
      <button type="submit" [disabled]="form.invalid">SAVE</button>
    </form>
  `,
})
export class Demo2Component  {
  form: FormGroup

  constructor(private fb: FormBuilder) {
    this.form = fb.group({
      isCompany: null,
      color: 'red',
      company: ['', [Validators.required, Validators.minLength(3)]],
      idNumber: [''],
    })

    this.form.get('isCompany')?.valueChanges
      .subscribe(isACompany => {
        if (isACompany) {
          // COMPANY
          this.form.get('company')?.enable();
          this.form.get('idNumber')?.setValidators([
             (c: any) => vatCodFiscValidator(c, 11)
          ])
        } else {
          // USER
          // this.form.get('company')?.setValue('')
          this.form.get('company')?.disable();
          this.form.get('idNumber')?.setValidators([
            Validators.required, (c: FormControl) => vatCodFiscValidator(c, 16)
          ])
        }
        this.form.get('idNumber')?.updateValueAndValidity();
      })
    // init isCompany
    this.form.get('isCompany')?.setValue(true)
  }

  save() {
    console.log(this.form.value)
    console.log(this.form.getRawValue())
  }

}
