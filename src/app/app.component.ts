import { Component } from '@angular/core';

@Component({
  selector: 'ava-root',
  template: `
    <!--The content below is only a placeholder and can be replaced.-->
    <button routerLink="demo1">demo1</button>
    <button routerLink="demo2">demo2</button>
    <button routerLink="demo3">demo3</button>
    <button routerLink="demo4">demo4</button>
    <button routerLink="demo5">demo5</button>
    <hr>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
  title = 'avanade-may-forms';
}
